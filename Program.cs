﻿using System;
using System.Collections.Generic;
using System.ServiceProcess;
using System.Text;

namespace InfiniRunner
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new InfiniRunnerService() 
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
