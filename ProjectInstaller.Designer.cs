﻿namespace InfiniRunner
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.serviceProcessInstallerInifiniRunner = new System.ServiceProcess.ServiceProcessInstaller();
            this.serviceInstallerInfiniRunner = new System.ServiceProcess.ServiceInstaller();
            // 
            // serviceProcessInstallerInifiniRunner
            // 
            this.serviceProcessInstallerInifiniRunner.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.serviceProcessInstallerInifiniRunner.Password = null;
            this.serviceProcessInstallerInifiniRunner.Username = null;
            this.serviceProcessInstallerInifiniRunner.AfterInstall += new System.Configuration.Install.InstallEventHandler(this.serviceProcessInstaller1_AfterInstall);
            // 
            // serviceInstallerInfiniRunner
            // 
            this.serviceInstallerInfiniRunner.Description = "InfiniRunner Installer";
            this.serviceInstallerInfiniRunner.DisplayName = "InfiniRunner";
            this.serviceInstallerInfiniRunner.ServiceName = "InfiniRunnerService";
            this.serviceInstallerInfiniRunner.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.serviceProcessInstallerInifiniRunner,
            this.serviceInstallerInfiniRunner});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller serviceProcessInstallerInifiniRunner;
        private System.ServiceProcess.ServiceInstaller serviceInstallerInfiniRunner;
    }
}