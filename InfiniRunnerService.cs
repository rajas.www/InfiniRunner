﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.ServiceProcess;
using System.Text;
using System.Configuration;
using System.Threading;

namespace InfiniRunner
{
    public partial class InfiniRunnerService : ServiceBase
    {
        static int retryCount = 0;
        static int retryFor = 5;
        static int processStartCount = 0;
        Thread _thread;

        public InfiniRunnerService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            base.OnStart(args);
            // Start worker thread to run inifinit service
            _thread = new Thread(InfiniRunerWorkerThread);
            _thread.Name = "InfiniRunner Worker Thread";
            _thread.IsBackground = true;
            _thread.Start();
        }

        protected override void OnStop()
        {
            _thread.Abort();
            base.OnStop();
        }

        private void InfiniRunerWorkerThread()
        {
            while (true) startProcess();
        }

         private static void startProcess()
        {
            try
            {
                String path = ConfigurationManager.AppSettings["processPath"];
                String arguments = ConfigurationManager.AppSettings["processArguments"];
                String processName = Path.GetFileName(path).Replace(".exe", "");
                bool IsProcessHidden = Boolean.Parse(ConfigurationManager.AppSettings["isProcessHidden"]);

                if (path.Length == 0) throw new Exception("Invalid path given");

                if (!KillProcessIfOpen(processName))
                {
                    ProcessStartInfo startInfo = new ProcessStartInfo(path);
                    //if(IsProcessHidden) startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    if (arguments.Length > 0) startInfo.Arguments = arguments;
                    Write("Starting process(number of starts = " + ++processStartCount + "): " + path + " " + arguments);

                    startInfo.UseShellExecute = false;
                    startInfo.RedirectStandardOutput = true;

                    Process process = new Process { StartInfo = startInfo, EnableRaisingEvents = true };
                    process.OutputDataReceived += new DataReceivedEventHandler(OutputHandler);

                    process.Start();
                    if (process.WaitForExit(Int32.MaxValue))
                        startProcess();
                }
                else throw new Exception("Process: " + processName + " is already running, application will try to close process in next attempt");
            }
            catch (Exception e)
            {
                retryCount++;
                Write("Failed to start process after " + retryCount + " tries \nMessage: " + e.Message);
                if (retryCount >= retryFor)
                {
                    Write("Application will now close");
                    Environment.Exit(0);
                }
            }

             
        }

         private static void OutputHandler(object sendingProcess,
             DataReceivedEventArgs outLine)
         {
             Write("Writing output\n");
             // Collect the sort command output. 
             if (!String.IsNullOrEmpty(outLine.Data))
             {
                 Write(outLine.Data);
             }
         }

        private static void Write(string message)
        {
            message = DateTime.Now.ToString() + " -- " + message;
            Debug.WriteLine(message);
            Console.WriteLine(message);
            using (StreamWriter w = File.AppendText(System.Reflection.Assembly.GetExecutingAssembly().Location + ".log.txt"))
            {
                w.WriteLine(message);
            }
        }

        public static bool IsProcessOpen(string name)
        {
            foreach (Process clsProcess in Process.GetProcesses())
            {
                if (clsProcess.ProcessName.Contains(name))
                {
                    return true;
                }
            }
            return false;
        }

        public static bool KillProcessIfOpen(string name)
        {
            foreach (Process clsProcess in Process.GetProcesses())
            {
                if (clsProcess.ProcessName.Contains(name))
                {
                    Write("Killing " + name);
                    clsProcess.Kill();
                    return true;
                }
            }
            return false;
        }

    }
}